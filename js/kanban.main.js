(function (Drupal) {
  'use strict';

  var custom_kanbans = $('.kanban_board');

  if (custom_kanbans.length > 0) {
    custom_kanbans.each(function (e) {
      var kanban = $(this);
      var actions = kanban.find('.kanban-actions');
      var board = kanban.find('.kanban-board');

      // var settings = kanban.find('.kanban-init').data('settings');
      // var titles = [];
      // var colors = [];
      // var items = [];
      // var ids = [];
      // $.each(settings, function(key,val){
      //   titles.push(val.title);
      //   ids.push(val.id);
      //   colors.push(val.color);
      //   $.each(val.items, function(key,val){
      //     items.push(val);
      //   });
      // });

      // kanban.find('.kanban-init').kanbanAttributeColumns({
      //   titles: titles,
      //   colours: colors,
      //   items: items,
      //   ids: ids
      // });

      board.on("sortstop", function (event, ui) {
        var task_id = ui.item[0].dataset.task;
        var column_id = ui.item[0].parentElement.dataset.block;

        var tasks = [];
        kanban.find("[data-block=" + column_id + "]").find(".cd_kanban_kanban_block_item").each(function () {
          tasks.push($(this).data("task"));
        });

        $.post(kanban.data("url"), {
          type: type,
          entity_type: kanban.data("entity-type"),
          column_changed: kanban.data("column-changed"),
          task_id: task_id,
          block_id: column_id,
          order: tasks
        }, function (result) {
          if (result.error == false) {
            $.growl.notice({
              title: result.title,
              message: result.message
            });
          } else {
            $.growl.error({
              title: translations.error_message,
              message: result.message
            });
          }
        }, "json");
      });
    });
  }


  Drupal.behaviors.kanbanMain = {
    attach: function (context, settings) {

    }
  }

})(Drupal);
