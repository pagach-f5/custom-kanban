<?php

namespace Drupal\custom_kanban\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Board entities.
 *
 * @ingroup custom_kanban
 */
class BoardEntityDeleteForm extends ContentEntityDeleteForm {


}
