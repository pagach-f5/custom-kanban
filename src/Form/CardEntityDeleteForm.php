<?php

namespace Drupal\custom_kanban\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Card entities.
 *
 * @ingroup custom_kanban
 */
class CardEntityDeleteForm extends ContentEntityDeleteForm {


}
