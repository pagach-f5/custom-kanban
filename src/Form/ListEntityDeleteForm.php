<?php

namespace Drupal\custom_kanban\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting List entities.
 *
 * @ingroup custom_kanban
 */
class ListEntityDeleteForm extends ContentEntityDeleteForm {


}
